#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 31 15:41:02 2017

@author: Alpha & Balour
"""

from PIL import Image
import os

fin = 0
Lien = input("Donner le chemin (absolu ou relatif) au fichier image souhaité: ")
image1 = Image.open(Lien)
(colonne,ligne) = image1.size
image1.show()
choix = input()


def ouvrirImage():
    Lien = input("Donner le chemin (absolu ou relatif) au fichier image souhaité: ")
    image1= Image.open(Lien)
    image1.show()
    
def ChargerImage():    
    image1= Image.open(Lien)
    image1.show()

def clearscreen():
    os.system("clr")

def InfoImage():
    print ("Format de l'image: ",image1.format)
    print ("Dimensions: ", image1.size)
    print ("Colorimétrie: ",  image1.mode)
            
def Negatif():
    (colonne,ligne) = image1.size
    imageNegative = Image.new(image1.mode, image1.size)
    for x in range(ligne):
        for y in range(colonne):
            pixelD = image1.getpixel((y,x))
            pixelF = (255 - pixelD[0], 255 - pixelD[1], 255 - pixelD[2])
            imageNegative.putpixel((y,x), pixelF)
    imageNegative.show()

def RotGauche():
    imageVerticale = Image.new(image1.mode, image1.size)
    imageVerticale = image1.transpose(Image.FLIP_LEFT_RIGHT)
    imageRotGauche = Image.new(image1.mode, (ligne,colonne))
    for x in range(ligne):
        for y in range(colonne):
            pixel = imageVerticale.getpixel((y,x))
            imageRotGauche.putpixel((x,y), pixel)
    imageRotGauche.show()

def RotDroite():
    imageHorizontale = Image.new(image1.mode, image1.size)
    imageHorizontale = image1.transpose(Image.FLIP_TOP_BOTTOM)
    imageRotDroite = Image.new(image1.mode, (ligne,colonne))
    for x in range(ligne):
        for y in range(colonne):
            pixel = imageHorizontale.getpixel((y,x))
            imageRotDroite.putpixel((x,y), pixel)
    imageRotDroite.show()

def SymVer():
    imageSymVerticale = Image.new(image1.mode, image1.size)
    for x in range(ligne):
        for y in range(colonne):
            pixel = image1.getpixel((y,x))
            imageSymVerticale.putpixel((colonne-y-1,x), pixel)
    imageSymVerticale.show()

def SymHor():
    imageH = Image.new(image1.mode, image1.size)
    for x in range(ligne):
        for y in range(colonne):
            pixel = image1.getpixel((y,x))
            imageH.putpixel((y,ligne-x-1), pixel)
    imageH.show()

def Retournement():
    imageVH = Image.new(image1.mode, image1.size)
    for x in range(ligne):
        for y in range(colonne):
            pixel = image1.getpixel((y,x))
            imageVH.putpixel((colonne-y-1,ligne-x-1), pixel)
    imageVH.show()

def sous_menuA():
    choixsm1="0"
    while choixsm1!="q":
        print(" \n Quel mode de rotation colorimètrique voulez-vous?")
        print("        1 - RGB - rotation de RGB->RGB")
        print("        2 - BGR - rotation de RGB->BGR")
        print("        3 - BRG - rotation de RGB->BRG")
        print("        4 - GBR - rotation de RGB->GBR")
        print("        Q - Revenir au menu principal")
        choixsm1=input("Choix : ")
        if choixsm1 == "1":
            image1.show()
        elif choixsm1 == "2":
            image=Image.open(Lien)
            L,l=image.size
            for i in range(L):
                for j in range(l):
                    rouge, vert, bleu = image.getpixel((i,j))
                    image.putpixel((i,j),(bleu, vert,rouge))
            image.show()
        elif choixsm1 == "3":
            image=Image.open(Lien)
            L,l=image.size
            for i in range(L):
                for j in range(l):
                    rouge, vert, bleu = image.getpixel((i,j))
                    image.putpixel((i,j),(bleu, rouge, vert))
            image.show()
        elif choixsm1 == "4":
            image=Image.open(Lien)
            L,l=image.size
            for i in range(L):
                for j in range(l):
                    rouge, vert, bleu = image.getpixel((i,j))
                    image.putpixel((i,j),(vert, bleu, rouge))
            image.show()
        elif choixsm1 == "q" or choixsm1 == "Q":
            exit
        else:
            print("Veuillez rentrer un choix valide")

 
def sous_menuB():
    choixsm2="0"
    while choixsm2!="q":
        print(" \n Quelle rotation physique voulez-vous ?")
        print("        1 - gauche")
        print("        2 - droite")
        print("        3 - retourner")
        print(" \n        Q - Revenir au menu principal")
        choixsm2=input("Choix: ")
        if choixsm2 == "1":
            RotGauche()
        elif choixsm2 == "2":
            RotDroite()
        elif choixsm2 == "3":
            Retournement()
        elif choixsm2 == "q" or choixsm2 == "Q":
            exit
        else:
            print("Veuillez rentrer un choix valide")
                  
def sous_menuC():
    choixsm3="0"
    while choixsm3!="q":
        print(" \n Quelle symétrie souhaitez-vous réaliser?")
        print("         - V - Symétrie verticale")
        print("         - H - Symétrie horizontale")
        print("         - VH - Double symétrie")
        print("         - Q - Revenir au menu principal")
        choixsm3=input("Choix: ")
        if choixsm3 == "V" or choixsm3 == "v":
            SymVer()
        elif choixsm3 == "H" or choixsm3 == "h":
            SymHor()
        elif choixsm3 == "VH" or choixsm3 == "vh" or choixsm3 == "vH" or choixsm3 == "Vh":
            Retournement()
        elif choixsm3 == "q" or choixsm3 == "Q":
            exit
        else:
            print("Veuillez rentrer un choix valide")
            

def filconducteur():
    choix="0"
    print(" \n Que voulez-vous faire?")
    print("         - 1 - Ouvrir une image")
    print("         - 2 - Afficher l'image chargée")
    print("         - 3 - Afficher les informations sur l'image")
    print("         - 4 - Mettre l'image en négatif")
    print("         - 5 - Effectuer une rotation des couleurs de l'image")
    print("         - 6 - Effectuer une rotation physique de l'image")
    print("         - 7 - Effectuer une transformation symétrique de l'image")
    print("         - 8 - Quitter le programme")
  
    choix = input("Choix : ")
    if choix == "1":
        clearscreen()
        ouvrirImage()
    elif choix == "2":
        clearscreen()
        image1= Image.open(Lien)
        image1.show()
    elif choix == "3":
        clearscreen()
        InfoImage()
    elif choix == "4":
        clearscreen()
        Negatif()
    elif choix == "5":
        clearscreen()
        sous_menuA()
    elif choix == "6":
        clearscreen()
        sous_menuB()
    elif choix == "7":
        clearscreen()
        sous_menuC()
    elif choix == "8":
        global fin
        clearscreen()
        fin=1
    else:
        print("Veuillez rentrer une valeur valide") 
            
while fin == 0:
    filconducteur()
    clearscreen()            

    
print("Au revoir et merci")
exit
